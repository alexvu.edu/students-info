## What is pull (merge) request?

![](images/scheme.png)

## How to create pull request
---
1. Create new branch based on master or main branch.
2. Make your practice work (the lab) on newly created branch.
3. Add clear commit message about work done. Use English for commit message for example: *"Add task 1 lab 10"*, *"Fix task 1 output"*
Use infinitive form of verbs and start from capital letter. 
4. Push your branch to repository.
5. Go to **Merge requests** section and press "New merge request" button.
6. Add to reviewers your teacher and your instructor [listed here](https://bit.ly/reviewers-for-srp).
